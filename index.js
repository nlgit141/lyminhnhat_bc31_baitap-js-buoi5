/** Bài tập 1: QUẢN LÝ TUYỂN SINH
 * INPUT:
 *
 * Khu vực ưu tiên:
 * - Khu vực A: +2
 * - Khu vực B +1
 * - Khu vực C: +0.5
 * Đối tượng ưu tiên:
 * - Đối tượng 1: +2.5;
 * - Đối tượng 1: +1.5;
 * - Đối tượng 1: +1;
 * 1 trong 3 môn = 0 : rớt
 *
 * CÁC BƯỚC XỬ LÝ:
 * tạo biến lưu điểm 3 môn
 *
 *
 *
 *
 * OUTPUT: tính tổng số điểm 3 môn theo điểm chuẩn => đậu hoặc rớt
 *
 *
 * **/
document.getElementById("txt-ketqua").addEventListener("click", function () {

  var diemChuanValue = document.getElementById("txt-diemchuan").value * 1;
  var khuVucUuTien = document.getElementById("khu-vuc").value;
  var doiTuong = document.getElementById("doi-tuong").value;
  var diemThu1Value = document.getElementById("txt-diemthu1").value * 1;
  var diemThu2Value = document.getElementById("txt-diemthu2").value * 1;
  var diemThu3Value = document.getElementById("txt-diemthu3").value * 1;
  var tongDiem = 0;
  tongDiem = diemThu1Value + diemThu2Value + diemThu3Value;
  switch (khuVucUuTien) {
    case "khu-A": {
      tongDiem += 2; break;
    }
    case "khu-B": {
      tongDiem += 1; break;
    }
    case "khu-C": {
      tongDiem += 0.5; break;
    }
  }
  switch (doiTuong) {
    case "ĐT1": {
      tongDiem += 2.5; break;
    }
    case "ĐT2": {
      tongDiem += 1.5; break;
    }
    case "ĐT3": {
      tongDiem += 1; break;
    }
  }

  if (diemThu1Value == 0 || diemThu2Value == 0 || diemThu3Value == 0) {
    console.log("kết quả: rớt do có điểm = 0");
    document.getElementById("ket-qua").innerHTML = "Kết quả: Rớt do có điểm = 0";
  } else if (tongDiem >= diemChuanValue) {
    console.log("kết quả: đậu", tongDiem);
    document.getElementById("ket-qua").innerHTML = `<span>Kết quả: Đậu ${tongDiem} điểm</span>`;
  } else if (tongDiem < diemChuanValue) {
    console.log("kết quả: rớt", tongDiem);
    document.getElementById("ket-qua").innerHTML = `<span>Kết quả: Rớt ${tongDiem} điểm</span>`;
  }
});




/**------------------------------------------ BÀI-2: Tính tiền điện---------------------------------------------------
 * mốc 1: <=50 thì 500đ/kw
 * mốc 2: >50 thì 650đ/kw
 * mốc 3: >=100 thì 850đ/kw
 * mốc 4: >=150 thì 1100đ/kw
 * mốc 5: 1300đ/kw
 */


var gia50KwDau = 500;
var gia50KwKe = 650;
var gia100KwKe = 850;
var gia150Kwke = 1100;
var giaKeTiep = 1300;

document.getElementById("tinh-tien-dien").addEventListener("click", function () {
  var ten = document.getElementById("txt-hoten").value;
  var kwValue = document.getElementById("txt-kw").value * 1;
  var soTienPhaiTra = 0;
  if (kwValue <= 50) {
    soTienPhaiTra = kwValue * gia50KwDau;
  }
  else if (kwValue <= 100) {
    soTienPhaiTra = (kwValue - 50) * gia50KwKe + (50 * gia50KwDau);
  }
  else if (kwValue <= 200) {
    soTienPhaiTra = (kwValue - 100) * gia100KwKe + (50 * gia50KwKe) + (50 * gia50KwDau);
  }
  else if (kwValue <= 350) {
    soTienPhaiTra = (kwValue - 200) * gia150Kwke + (100 * gia100KwKe) + (50 * gia50KwKe) + (50 * gia50KwDau);
  }
  else {
    soTienPhaiTra = (kwValue - 350) * giaKeTiep + (150 * gia150Kwke) + (100 * gia100KwKe) + (50 * gia50KwKe) + (50 * gia50KwDau);
  };

  // document.getElementById("tien-dien").innerHTML = `<div>Tên: ${ten} số tiền: ${Intl.NumberFormat().format(soTienPhaiTra) + " VNĐ"}</div>`;
  document.getElementById("tien-dien").innerText = "Tên: " + ten + ". " + "Số tiền: " + Intl.NumberFormat().format(soTienPhaiTra) + " vnđ";
});


//  document.getElementById("tinh-tien-dien").addEventListener("click", function () {
//    var ten = document.getElementById("txt-hoten").value;
//    var kwValue = document.getElementById("txt-kw").value * 1;
//    var soTienPhaiTra = 0;
//    if (kwValue <= 50) {
//      soTienPhaiTra = kwValue * 500;
//    }
//    else if (kwValue <= 100) {
//      soTienPhaiTra = (kwValue - 50) * 650 + (50 * 500);
//    }
//    else if (kwValue <= 200) {
//      soTienPhaiTra = (kwValue - 100) * 850 + (50 * 650) + (50 * 500);
//    }
//    else if (kwValue <= 350) {
//      soTienPhaiTra = (kwValue - 200) * 1100 + (100 * 850) + (50 * 650) + (50 * 500);
//    }
//    else {
//      soTienPhaiTra = (kwValue - 350) * 1300 + (150 * 1100) + (100 * 850) + (50 * 650) + (50 * 500);
//    };

//    document.getElementById("tien-dien").innerHTML = `<div>số tiền: ${Intl.NumberFormat().format(soTienPhaiTra) + " VNĐ"}</div>`;
//  });